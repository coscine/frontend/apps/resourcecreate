module.exports = {
  devServer: {
    disableHostCheck: true,
  },
  publicPath: "./",
  configureWebpack: {
    devtool: "source-map",
    devServer: {
      port: 7936,
    },
  },
  filenameHashing: false,
  chainWebpack: (config) => {
    config.optimization.delete("splitChunks");
  },
  css: {
    extract: false,
  },
  runtimeCompiler: true,
};
