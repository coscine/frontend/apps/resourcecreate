import jQuery from "jquery";
import BootstrapVue, { componentsPlugin } from "bootstrap-vue";
import Vue, { Component, ComponentOptions, VNode } from "vue";
import ResourceCreationApp from "./ResourceCreationApp.vue";
import VueI18n from "vue-i18n";
import { LanguageUtil } from "@coscine/app-util";
import VueRouter from "vue-router";

import Setup from "./components/Setup.vue";
import GeneralInfo from "./components/GeneralInfo.vue";
import Metadata from "./components/Metadata.vue";
import Overview from "./components/Overview.vue";

Vue.config.productionTip = false;
Vue.use(BootstrapVue);
Vue.use(VueI18n);
Vue.use(VueRouter);

const routes: [
  { path: string; component: any },
  { path: string; component: any },
  { path: string; component: any },
  { path: string; component: any },
  { path: string; component: any }
] = [
  {
    path: "/",
    component: ResourceCreationApp,
  },
  {
    path: "/setup",
    component: Setup,
  },
  {
    path: "/generalInfo",
    component: GeneralInfo,
  },
  {
    path: "/metadata",
    component: Metadata,
  },
  {
    path: "/overview",
    component: Overview,
  },
];

const router = new VueRouter({
  routes,
});

jQuery(() => {
  const i18n = new VueI18n({
    locale: LanguageUtil.getLanguage(),
    messages: coscine.i18n.resourcecreation,
    silentFallbackWarn: true,
  });
  new Vue({
    render: (h) => h(ResourceCreationApp),
    i18n,
    router,
  }).$mount("resourcecreate");
});

declare global {
  interface String {
    firstToUpperCase(): string;
  }
}

String.prototype.firstToUpperCase = function () {
  return this && this.charAt(0).toUpperCase() + this.slice(1);
};
